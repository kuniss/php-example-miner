<?php

namespace App\Service;

use Symfony\Component\Cache\Adapter\TagAwareAdapter;

/**
 * Class EntityCacheService
 */
class EntityCacheService
{
    /**
     * @var TagAwareAdapter
     */
    private $tagAwareAdapter;

    /**
     * EntityCacheService constructor.
     *
     * @param TagAwareAdapter $tagAwareAdapter
     */
    public function __construct(TagAwareAdapter $tagAwareAdapter)
    {
        $this->tagAwareAdapter = $tagAwareAdapter;
    }

    /**
     * Store entity object to cache if not exists
     * - convert entity to json
     *
     * @param ICacheableEntity $entity
     * @param string $cacheTag
     * @param bool $forceIfExist
     * @return bool
     */
    public function storeEntity(ICacheableEntity $entity, string $cacheTag, bool $forceIfExist): bool
    {
        $item = $this->tagAwareAdapter->getItem(
            $entity->getCacheId()
        );

        if ($forceIfExist == true || $item->isHit() == false) {
            $item->set(json_encode($entity));
            $item->tag($cacheTag);
            return $this->tagAwareAdapter->save($item);
        }
        return false;
    }

  /**
   * Check if is entity already stored
   *
   * @param ICacheableEntity $entity
   * @return bool
   */
    public function isEntityStored(ICacheableEntity $entity): bool
    {
        return $this->tagAwareAdapter->hasItem(
            $entity->getCacheId()
        );
    }
}
