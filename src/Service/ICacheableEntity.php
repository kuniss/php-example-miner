<?php

namespace App\Service;

/**
 * Interface ICacheableEntity
 */
interface ICacheableEntity
{
    public function getCacheId();
}