<?php

namespace App\Exception;

/**
 * Class RuntimeException
 */
class RuntimeException extends Exception
{
    /**
     * Factory method httpRequestFailed
     *
     * @param string $url
     * @return RuntimeException
     */
    public static function httpRequestFailed(string $url): self
    {
        return new static(
            sprintf("Request execution to url '%s' failed.", $url)
        );
    }
}
