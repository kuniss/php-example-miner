<?php

namespace App\Exception;

/**
 * Class UnexpectedValueException
 */
class UnexpectedValueException extends Exception
{
    /**
     * Self factory method - invalid or empty entity
     *
     * @param string $entityName
     * @return self
     */
    public static function invalidEntity(string $entityName): self
    {
        return new static(sprintf(
            "Entity with name '%s' is empty or has invalid values.", $entityName
        ));
    }

    /**
     * Self factory method - value is empty or not defined
     *
     * @param string $valueName
     * @return self
     */
    public static function valueIsEmpty(string $valueName): self
    {
        return new static(sprintf(
            "Required value '%s' is not defined or empty.", $valueName
        ));
    }
}
