<?php

namespace App\Exception;

/**
 * Class OutOfBoundsException
 */
class OutOfBoundsException extends RuntimeException
{ }
