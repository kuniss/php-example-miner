<?php

namespace App\Factory;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use App\Service\EntityCacheService;

/**
 * Abstract class EntityCacheServiceFactory
 */
abstract class EntityCacheServiceFactory
{
    /**
     * @var string
     */
    private const NAMESPACE_DATA = 'articlesData';

    /**
     * @var string
     */
    private const NAMESPACE_TAGS = 'articlesTags';

    /**
     * EntityCacheService factory method, configure TagAwareAdapter namespace
     *
     * @return EntityCacheService
     */
    public static function create(): EntityCacheService
    {
        $init = new EntityCacheService(
            new TagAwareAdapter(
                new FilesystemAdapter(self::NAMESPACE_DATA),
                new FilesystemAdapter(self::NAMESPACE_TAGS)
            )
        );
        return $init;
    }
}
