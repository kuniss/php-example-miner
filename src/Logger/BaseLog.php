<?php

namespace App\Logger;

use Psr\Log\LoggerInterface;

/**
 * Class BaseLog
 */
abstract class BaseLog
{
    /**
    * @var LoggerInterface
    */
    protected $logger;

    /**
     * CmdLogChannel constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Store exception info
     *
     * @param Throwable $e
     */
    public function exception(\Throwable $e): void
    {
          $this->logger->error($e->getMessage(), [
              'trace' => $e->getTraceAsString()
          ]);
    }
}
