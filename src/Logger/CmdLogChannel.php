<?php

namespace App\Logger;

/**
 * Class CmdLogChannel
 */
class CmdLogChannel extends BaseLog
{
    /**
     * @var array
     */
    private $processTimeContainer = [];

    /**
     * Store basic info about start processing command
     *
     * @param string $commandName
     */
    public function startProcess(string $commandName): void
    {
        $this->processTimeContainer[$commandName] = microtime(true);
        $this->logger->info(sprintf('START COMMAND: %s', $commandName));
    }

    /**
     * Store info about end of process with during time
     *
     * @param string $commandName
     */
    public function endProcess(string $commandName): void
    {
        $this->logger->info(sprintf(
          'END COMMAD: %s (%d)', $commandName, $this->getCurrentProcessTime($commandName)
        ));
    }

    /**
     * Get length of current process by name
     *
     * @param string $commandName
     * @return float
     */
    public function getCurrentProcessTime(string $commandName): float
    {
        if (isset($this->processTimeContainer[$commandName]) == false) {
            return 0;
        }
        return (microtime(true) - $this->processTimeContainer[$commandName]);
    }
}
