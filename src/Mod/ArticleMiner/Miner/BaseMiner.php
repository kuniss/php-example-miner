<?php

namespace App\Mod\ArticleMiner\Miner;

use App\Exception\UnexpectedValueException;
use App\Mod\ArticleMiner\Provider\BaseProvider;

/**
 * Abstract class BaseMiner
 */
abstract class BaseMiner
{
    /**
     * @var BaseProvider
     */
    protected $provider;

    /**
     * Getter provider name
     *
     * @return string
     * @throws UnexpectedValueException do not set provider
     */
    public function getProviderName(): string
    {
        if (isset($this->provider) === false) {
            throw UnexpectedValueException::valueIsEmpty('provider');
        }
        return $this->provider->getName();
    }
}
