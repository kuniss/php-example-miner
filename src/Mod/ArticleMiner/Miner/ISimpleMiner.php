<?php

namespace App\Mod\ArticleMiner\Miner;

/**
 * Interface ISimpleMiner
 */
interface ISimpleMiner
{
    public function work(callable $workResult): void;
}
