<?php

namespace App\Mod\ArticleMiner\Miner;

use App\Mod\ArticleMiner\Provider\IHackerNewsArticleProvider;
use App\Mod\ArticleMiner\Entity\ArticleEntity;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Exception\UnexpectedValueException;
use App\Logger\MiningLogChannel;

/**
 * Class HackerNewsMiner
 * - fetch all articles via provider and report results
 */
class HackerNewsMiner extends BaseMiner implements ISimpleMiner
{
    /**
     * @var MiningLogChannel
     */
    private $logger;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * HackerNewsMiner constructor.
     *
     * @param MiningLogChannel $logger
     * @param ValidatorInterface $validator
     * @param IHackerNewsArticleProvider $hackerNewsArticleProvider
     */
    public function __construct(MiningLogChannel $logger , ValidatorInterface $validator, IHackerNewsArticleProvider $hackerNewsArticleProvider)
    {
        $this->logger = $logger;
        $this->validator = $validator;
        $this->provider = $hackerNewsArticleProvider;
    }

    /**
     * Getting articles and use $workResult callback to present each Article entity
     *
     * @param callable $workResult (ArticleEntity, ..)
     */
    public function work(callable $workResult): void
    {
        $listIds = $this->provider->getArticlesIds();

        foreach ($listIds as $articleId) {
            try {
                $articleEntity = $this->getArticleById($articleId);

                call_user_func(
                    $workResult,
                    $articleEntity
                );

            } catch (\Throwable $e) {
                $this->logger->exception($e);
            }
        }
    }

    /**
     * Get article data via provider, check entity validity
     *
     * @param int $id
     * @return ArticleEntity
     * @throws UnexpectedValueException invalid entity
     */
    private function getArticleById(int $id): ArticleEntity
    {
        $entity = $this->provider->getArticleById($id);

        if ($this->validator->validate($entity)->count() > 0) {
            throw UnexpectedValueException::invalidEntity(ArticleEntity::class);
        }
        return $entity;
    }
}
