<?php

namespace App\Mod\ArticleMiner\Miner;

/**
 * Class MinersContainer
 */
class MinersContainer
{
    /**
     * @var array
     */
    private $minerContainer = [];

    /**
     * Add objects into the container
     *
     * @param BaseMiner $miner
     */
    public function add(BaseMiner $miner): void
    {
        $this->minerContainer[] = $miner;
    }

    /**
     * Get all stored objects
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->minerContainer;
    }
}
