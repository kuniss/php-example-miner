<?php

namespace App\Mod\ArticleMiner\Provider;

use App\Exception\OutOfBoundsException;
use App\Mod\ArticleMiner\Entity\ArticleEntity;
use App\Utils\CurlRequest;
use Psr\Log\LoggerInterface;

/**
 * Class HackerNewsV0ArticleProvider
 * - api hacker news proxy "V0"
 */
class HackerNewsV0ArticleProvider extends BaseProvider implements IHackerNewsArticleProvider
{
    /**
     * @var string
     */
     protected const NAME = 'hackerNews';

    /**
     * @var string
     */
    private const URL_NEW_STORIES = '/v0/newstories.json',
        URL_ITEM_HEADER = '/v0/item/%d.json';

    /**
     * @var string
     */
    private const KEY_ATTR_PRINT_PRETTY = '?print=pretty';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CurlRequest
     */
    private $curlRequest;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * HackerNewsV0ArticleProvider constructor.
     *
     * @param LoggerInterface $logger
     * @param CurlRequest $curlRequest
     */
    public function __construct(LoggerInterface $logger, CurlRequest $curlRequest)
    {
        $this->logger = $logger;
        $this->curlRequest = $curlRequest;
    }

    /**
     * Variable setter baseUrl
     *
     * @param string $url
     */
    public function setBaseUrl(string $url): void
    {
        $this->baseUrl = $url;
    }

    /**
     * Get list of ids with attribute (url - print pretty)
     *
     * @return array
     */
    public function getArticlesIds(): array
    {
        return $this->getContent(
            $this->getFullUrlPrintPretty(self::URL_NEW_STORIES)
        );
    }

    /**
     * Get article data and create entity (url - print pretty)
     *
     * @param int $id
     * @return ArticleEntity
     * @throws OutOfBoundsException bad input id
     */
    public function getArticleById(int $id): ArticleEntity
    {
        if (($id > 0) == false) {
            throw new OutOfBoundsException('Article identifier is not valid.');
        }
        $response = $this->getContent(
            $this->getFullUrlPrintPretty(sprintf(self::URL_ITEM_HEADER, $id))
        );

        $response['id'] = $id;
        $response['url'] = $this->getFullUrlPrintPretty(sprintf(self::URL_ITEM_HEADER, $response['id']));
        $response['createdDate'] = (new \DateTime())->setTimestamp($response['time']);

        return ArticleEntity::createByArray($response);
    }

    /**
     * Wrapper method - get data from another source via curl
     *
     * @param string $url
     * @return array
     */
    private function getContent(string $url): array
    {
        try {
            $response = $this->curlRequest->getJsonContent($url);

        } catch (\Throwable $e) {
            $this->logger->warning($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
        return $response;
    }

    /**
     * Generate valid request url with args: print=pretty
     *
     * @param string $pathWithoutArgs
     * @return string
     */
    private function getFullUrlPrintPretty(string $pathWithoutArgs): string
    {
        return (
            rtrim($this->baseUrl, DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . trim($pathWithoutArgs, DIRECTORY_SEPARATOR)
            . self::KEY_ATTR_PRINT_PRETTY
        );
    }
}
