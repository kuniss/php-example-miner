<?php

namespace App\Mod\ArticleMiner\Provider;

use App\Mod\ArticleMiner\Entity\ArticleEntity;

/**
 * Interface IHackerNewsArticleProvider
 */
interface IHackerNewsArticleProvider
{
    public function getArticlesIds(): array;
    public function getArticleById(int $id): ArticleEntity;
}
