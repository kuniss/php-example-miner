<?php

namespace App\Mod\ArticleMiner\Provider;

/**
 * Abstract class BaseProvider
 */
abstract class BaseProvider
{
    /**
     * Getter provider name, static constant call
     *
     * @return string
     */
    public function getName(): string
    {
        return static::NAME;
    }
}
