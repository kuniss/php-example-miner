<?php

namespace App\Mod\ArticleMiner\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Mod\ArticleMiner\Service\MinersManagerService;
use App\Mod\ArticleMiner\Miner\MinersContainer;
use App\Logger\CmdLogChannel;

/**
 * Class RunMinersCommand
 * - will used all configured miners to our work
 * - log actions via CmdLogChannel
 */
class RunMinersCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:articleMiner:runMiners';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MinersManagerService
     */
    private $minersManagerService;

    /**
     * @var MinersContainer
     */
    private $minersContainer;

    /**
     * RunMinersCommand constructor.
     *
     * @param CmdLogChannel $logger
     * @param MinersManagerService $minersManagerService
     * @param MinersContainer $minersContainer
     */
    public function __construct(CmdLogChannel $logger, MinersManagerService $minersManagerService, MinersContainer $minersContainer)
    {
        $this->logger = $logger;
        $this->minersManagerService = $minersManagerService;
        $this->minersContainer = $minersContainer;

        parent::__construct();
    }

    /**
     * Execute method - call defined miners to action
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('START');
        $this->logger->startProcess(self::$defaultName);

        try {
            // run
            $this->minersManagerService->runMiners(
                $this->minersContainer
            );

        } catch (\Throwable $e) {
            $this->logger->exception($e);
            $output->writeln($e->getMessage());
        }

        $this->logger->endProcess(self::$defaultName);

        $output->writeln(sprintf('TIME: %d', $this->logger->getCurrentProcessTime(self::$defaultName)));
        $output->writeln('FINISH');
        $output->writeln('---------------------------------------------');
    }
}
