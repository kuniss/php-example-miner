<?php

namespace App\Mod\ArticleMiner\Factory;

use App\Utils\CurlRequest;
use App\Mod\ArticleMiner\Provider\HackerNewsV0ArticleProvider;
use Psr\Log\LoggerInterface;

/**
 * Abstract class HackerNewsArticleProviderFactory
 */
abstract class HackerNewsArticleProviderFactory
{
  /**
   * Factory method with default setup - "VO"
   *
   * @param LoggerInterface $logger
   * @param CurlRequest $curlRequest
   * @param string $providerBaseUrl
   * @return HackerNewsV0ArticleProvider
   */
  public static function createV0(LoggerInterface $logger, CurlRequest $curlRequest, string $providerBaseUrl): HackerNewsV0ArticleProvider
  {
      $init = new HackerNewsV0ArticleProvider($logger, $curlRequest);
      $init->setBaseUrl($providerBaseUrl);
      return $init;
  }
}
