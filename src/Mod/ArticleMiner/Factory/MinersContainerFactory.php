<?php

namespace App\Mod\ArticleMiner\Factory;

use Psr\Container\ContainerInterface;
use App\Mod\ArticleMiner\Miner\MinersContainer;

/**
 * Abstract class MinersContainerFactory
 */
abstract class MinersContainerFactory
{
    /**
     * Factory method
     * - init new container and store miners objects into the container by list
     *
     * @param ContainerInterface $container
     * @param array $minersList
     * @return MinersContainer
     */
    public static function createByListOfMiners(ContainerInterface $container, array $minersList): MinersContainer
    {
        $init = new MinersContainer();

        foreach ($minersList as $minerName) {
            $init->add($container->get($minerName));
        }
        return $init;
    }
}
