<?php

namespace App\Mod\ArticleMiner\Entity;

use App\Service\ICacheableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ArticleEntity
 */
class ArticleEntity implements ICacheableEntity
{
    /**
     * @var string
     */
    private const CACHE_KEY_PATTERN = 'article.%d';

    /**
      * @Assert\Type("int")
      * @Assert\NotNull()
     */
    public $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     */
    public $title;

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     */
    public $url;

    /**
     * @Assert\DateTime()
     * @Assert\NotNull()
     */
    public $createdDate;

    /**
     * Getter entity identifier
     *
     * @return string
     */
    public function getCacheId(): string
    {
        return sprintf(self::CACHE_KEY_PATTERN, $this->id);
    }

    /**
     * Self factory method
     *
     * @param array $data
     * @return self
     */
    public static function createByArray(array $data): self
    {
        $entity = new static();

        if (empty($data) == false) {
            $entity->id = $data['id'];
            $entity->title = $data['title'];
            $entity->url = $data['url'];
            $entity->createdDate = $data['createdDate'];
        }
        return $entity;
    }
}
