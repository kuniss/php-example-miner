<?php

namespace App\Mod\ArticleMiner\Service;

use App\Mod\ArticleMiner\Miner\MinersContainer;
use App\Mod\ArticleMiner\Miner\ISimpleMiner;
use App\Logger\MiningLogChannel;
use App\Service\EntityCacheService;
use App\Service\ICacheableEntity;

/**
 * Class MinersManagerService
 * - will wakeup miners from container and their work will be cached
 */
class MinersManagerService
{
    /**
     * @var MiningLogChannel
     */
    private $logger;

    /**
     * @var EntityCacheService
     */
    private $entityCacheService;

    /**
     * MinersManagerService constructor.
     *
     * @param MiningLogChannel $logger
     * @param EntityCacheService $entityCacheService
     */
    public function __construct(MiningLogChannel $logger, EntityCacheService $entityCacheService)
    {
        $this->logger = $logger;
        $this->entityCacheService = $entityCacheService;
    }

    /**
     * Run miners method - store articles by each miner
     *
     * @param MinersContainer $minersContainer
     */
    public function runMiners(MinersContainer $minersContainer): void
    {
        foreach ($minersContainer->getAll() as $miner) {
            try {
              $this->minerWork($miner);

            } catch (\Throwable $e) {
                $this->logger->exception($e);
            }
        }
    }

    /**
     * Store all articles to cache
     *
     * @param ISimpleMiner $miner
     */
    private function minerWork(ISimpleMiner $miner): void
    {
        $miner->work(function (ICacheableEntity $result) use ($miner) {

            if ($this->entityCacheService->isEntityStored($result) == false) {

                $this->entityCacheService->storeEntity(
                    $result,
                    $miner->getProviderName(),
                    false
                );
            }
        });
    }
}
