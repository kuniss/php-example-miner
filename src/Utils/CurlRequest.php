<?php

namespace App\Utils;

use App\Exception\RuntimeException;
use App\Exception\UnexpectedValueException;

/**
 * @deprecated use symfony http client
 * Class CurlRequest
 *
 * Implemented:
 * - create curl request and decode json response
 */
class CurlRequest
{
    /**
     * Get content from json response
     *
     * @return array
     * @throws UnexpectedValueException empty url or bad json format
     */
    public function getJsonContent(string $url): array
    {
        if (empty($url)) {
            throw new UnexpectedValueException('Url is empty or invalid.');
        }
        $content = $this->curlExec(
            $url,
            ['Content-type: application/json']
        );

        $decodedDate = json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new UnexpectedValueException('Invalid json format.');
        }
        return $decodedDate;
    }

    /**
     * Curl exec
     *
     * @param string $url
     * @param array $httpHeader
     * @return string
     * @throws RuntimeException failed request
     */
    private function curlExec(string $url, array $httpHeader): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        if ($response === false) {
            throw RuntimeException::httpRequestFailed($url, curl_error($ch));
        } else {
            curl_close($ch);
        }
        return $response;
    }
}
