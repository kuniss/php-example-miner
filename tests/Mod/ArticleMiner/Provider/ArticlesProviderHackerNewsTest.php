<?php

namespace App\Tests\Mod\ArticleMiner\Provider;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Utils\CurlRequest;
use App\Mod\ArticleMiner\Provider\HackerNewsV0ArticleProvider;
use mysql_xdevapi\Exception;
use Psr\Log\LoggerInterface;

/**
 * Class HackerNewsV0ArticleProviderTest
 */
class HackerNewsV0ArticleProviderTest extends MockeryTestCase
{
    public function testGetArticlesListIds()
    {
        $ids = [2343, 3422];

        $mockCurlRequest = \Mockery::mock(CurlRequest::class);
        $mockCurlRequest->shouldReceive('getJsonContent')
        ->twice()
        ->with('www.test.test/v0/newstories.json?print=pretty')
        ->andReturn($ids);

        $mockLogger = \Mockery::mock(LoggerInterface::class);

        $hackerNewsProvider = new HackerNewsV0ArticleProvider($mockLogger, $mockCurlRequest);
        $hackerNewsProvider->setBaseUrl('www.test.test');

        // asserts
        $this->assertCount(2, $hackerNewsProvider->getArticlesIds());
        $this->assertEquals($ids[1], $hackerNewsProvider->getArticlesIds()[1]);
    }

    public function testGetArticleById()
    {
        $articleData = [
            'title' => 'article test 12',
            'time' => 1546300800,
        ];

        $mockCurlRequest = \Mockery::mock(CurlRequest::class);
        $mockCurlRequest->shouldReceive('getJsonContent')
        ->once()
        ->with('www.test.test/v0/item/12.json?print=pretty')
        ->andReturn($articleData);

        $mockLogger = \Mockery::mock(LoggerInterface::class);

        $hackerNewsProvider = new HackerNewsV0ArticleProvider($mockLogger, $mockCurlRequest);
        $hackerNewsProvider->setBaseUrl('www.test.test');
        $articleEntity = $hackerNewsProvider->getArticleById(12);

        $this->assertEquals(
            [
                'id' => 12,
                'url' => 'www.test.test/v0/item/12.json?print=pretty',
                'title' => 'article test 12',
                'createdDate' => new \DateTime('2019-01-01')
            ],
            get_object_vars($articleEntity)
        );
    }

    public function testLoggerErrorContentCall()
    {
        $this->expectException(\Exception::class);

        $mockCurlRequest = \Mockery::mock(CurlRequest::class);
        $mockCurlRequest->shouldReceive('getJsonContent')
        ->andThrow(new \Exception());

        $mockLogger = \Mockery::mock(LoggerInterface::class);
        $mockLogger->shouldReceive('warning')
        ->once();

        $hackerNewsProvider = new HackerNewsV0ArticleProvider($mockLogger, $mockCurlRequest);
        $hackerNewsProvider->setBaseUrl('www.test.test');
        $hackerNewsProvider->getArticlesIds();
    }
}
