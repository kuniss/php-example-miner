<?php

namespace App\Tests\Mod\ArticleMiner\Service;

use App\Logger\MiningLogChannel;
use App\Mod\ArticleMiner\Miner\BaseMiner;
use App\Mod\ArticleMiner\Miner\ISimpleMiner;
use App\Mod\ArticleMiner\Miner\MinersContainer;
use App\Mod\ArticleMiner\Provider\BaseProvider;
use App\Mod\ArticleMiner\Service\MinersManagerService;
use App\Service\EntityCacheService;
use App\Service\ICacheableEntity;
use Mockery\Adapter\Phpunit\MockeryTestCase;

/**
 * Class MinersManagerServiceTest
 */
class MinersManagerServiceTest extends MockeryTestCase
{

    public function testCacheMinerWork()
    {
        $mockEntity = \Mockery::mock(ICacheableEntity::class);

        $mockSimpleMiner = \Mockery::mock(BaseMiner::class, ISimpleMiner::class);
        $mockSimpleMiner->shouldReceive('work')
        ->twice()
        ->andReturnUsing(function (callable $closure) use ($mockEntity) {
            call_user_func($closure, $mockEntity);
        });

        $mockSimpleMiner->shouldReceive('getProviderName')
        ->once()
        ->andReturn('testProvider');

        $mockMinersContainer = \Mockery::mock(MinersContainer::class);
        $mockMinersContainer->shouldReceive('getAll')
        ->once()
        ->andReturn([$mockSimpleMiner, $mockSimpleMiner]);

        $mockLogger = \Mockery::mock(MiningLogChannel::class);
        $mockLogger->shouldNotReceive('exception');

        $mockEntityCacheService = \Mockery::mock(EntityCacheService::class);
        $mockEntityCacheService->shouldReceive('isEntityStored')
        ->once()
        ->andReturn(false);

        $mockEntityCacheService->shouldReceive('isEntityStored')
        ->once()
        ->andReturn(true);

        $mockEntityCacheService->shouldReceive('storeEntity')
        ->once();

        // init service
        $minersManagerService = new MinersManagerService($mockLogger, $mockEntityCacheService);
        $minersManagerService->runMiners($mockMinersContainer);
    }

    public function testLogErrors()
    {
        $mockSimpleMiner = \Mockery::mock(ISimpleMiner::class);
        $mockSimpleMiner->shouldReceive('work')
        ->once()
        ->andThrow(\Exception::class);

        $mockMinersContainer = \Mockery::mock(MinersContainer::class);
        $mockMinersContainer->shouldReceive('getAll')
        ->once()
        ->andReturn([$mockSimpleMiner]);

        $mockLogger = \Mockery::mock(MiningLogChannel::class);
        $mockLogger->shouldReceive('exception')
        ->once();

        $mockEntityCacheService = \Mockery::mock(EntityCacheService::class);

        // init service
        $minersManagerService = new MinersManagerService($mockLogger, $mockEntityCacheService);
        $minersManagerService->runMiners($mockMinersContainer);
    }
}
