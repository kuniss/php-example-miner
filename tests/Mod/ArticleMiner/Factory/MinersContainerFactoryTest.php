<?php

namespace App\Tests\Mod\ArticleMiner\Factory;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Factory\MinersContainerFactory;
use App\Mod\ArticleMiner\Miner\MinersContainer;
use App\Mod\ArticleMiner\Miner\BaseMiner;
use Psr\Container\ContainerInterface;

/**
 * Class MinersContainerFactoryTest
 */
class MinersContainerFactoryTest extends MockeryTestCase
{
    public function testFactoryMethodByMinersList()
    {
        $mockContainerInterface = \Mockery::mock(ContainerInterface::class);
        $mockContainerInterface->shouldReceive('get')
        ->times(2)
        ->andReturn(\Mockery::mock(BaseMiner::class));

        $init = MinersContainerFactory::createByListOfMiners(
            $mockContainerInterface,
            ['miner1', 'miner2']
        );

        $this->assertEquals(MinersContainer::class, get_class($init));
    }
}
