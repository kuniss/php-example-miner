<?php

namespace App\Tests\Mod\ArticleMiner\Factory;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Factory\HackerNewsArticleProviderFactory;
use App\Mod\ArticleMiner\Provider\HackerNewsV0ArticleProvider;
use App\Utils\CurlRequest;
use Psr\Log\LoggerInterface;

/**
 * Class HackerNewsArticleProviderFactoryTest
 */
class HackerNewsArticleProviderFactoryTest extends MockeryTestCase
{
    public function testV0FactoryMethod()
    {
        $mockCurlRequest = \Mockery::mock(CurlRequest::class);
        $mockLogger = \Mockery::mock(LoggerInterface::class);

        $init = HackerNewsArticleProviderFactory::createV0(
            $mockLogger,
            $mockCurlRequest,
            'providerFoo'
        );

        $this->assertEquals(HackerNewsV0ArticleProvider::class, get_class($init));
    }
}
