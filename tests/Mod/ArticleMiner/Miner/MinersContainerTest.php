<?php

namespace App\Tests\Mod\ArticleMiner\Miner;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Miner\BaseMiner;
use App\Mod\ArticleMiner\Miner\MinersContainer;

/**
 * Class MinerContainerTest
 */
class MinerContainerTest extends MockeryTestCase
{
    public function testGetAddedObjects()
    {
        $mockBaseMiner = \Mockery::mock(BaseMiner::class);

        $container = new MinersContainer();
        $container->add($mockBaseMiner);

        $this->assertCount(1, $container->getAll());
        $this->assertContains($mockBaseMiner, $container->getAll());
        $this->assertEquals($mockBaseMiner, $container->getAll()[0]);
    }
}
