<?php

namespace App\Tests\Mod\ArticleMiner\Miner;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Miner\HackerNewsMiner;
use App\Tests\Mod\ArticleMiner\MockTraits\ValidatorInterfaceMock;
use App\Tests\Mod\ArticleMiner\MockTraits\IHackerNewsArticleProviderMock;
use App\Logger\MiningLogChannel;

/**
 * Class HackerNewsMinerTest
 */
class HackerNewsMinerTest extends MockeryTestCase
{
    use ValidatorInterfaceMock;
    use IHackerNewsArticleProviderMock;

    public function testWorkMethod()
    {
        $articleEntityData = [
            'id' => 23,
            'title' => 'article 23 mock',
            'url' => '/article/23/mock',
            'createdDate' => new \DateTime('2019-01-01'),
        ];

        $mockLogger = \Mockery::mock(MiningLogChannel::class);

        $init = new HackerNewsMiner(
            $mockLogger,
            $this->getValidatorInterfaceMock(0),
            $this->getIHackerNewsArticleProviderMock($articleEntityData)
        );

        $init->work(function ($articleEntityResult) use ($articleEntityData) {
            // assert
            $this->assertEquals($articleEntityData, get_object_vars($articleEntityResult));
        });
    }
}
