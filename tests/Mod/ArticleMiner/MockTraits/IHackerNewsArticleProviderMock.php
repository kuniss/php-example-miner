<?php

namespace App\Tests\Mod\ArticleMiner\MockTraits;

use App\Mod\ArticleMiner\Provider\BaseProvider;
use App\Mod\ArticleMiner\Provider\IHackerNewsArticleProvider;
use App\Mod\ArticleMiner\Entity\ArticleEntity;

/**
 * Trait IHackerNewsArticleProviderMock
 */
trait IHackerNewsArticleProviderMock
{
    /**
     * Create IHackerNewsArticleProvider mock object with BaseProvider parent object and mock article entity
     *
     * @param array $entityData
     * @return IHackerNewsArticleProvider
     */
    private function getIHackerNewsArticleProviderMock(array $entityData): IHackerNewsArticleProvider
    {
        $articleEntity = ArticleEntity::createByArray($entityData);

        $mockProvider = \Mockery::mock(
            BaseProvider::class, IHackerNewsArticleProvider::class
        );
        $mockProvider->shouldReceive('getArticleById')
        ->with($entityData['id'])
        ->once()
        ->andReturn($articleEntity);

        $mockProvider->shouldReceive('getArticlesIds')
        ->once()
        ->andReturn([$entityData['id']]);

        return $mockProvider;
    }
}
