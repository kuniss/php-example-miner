<?php

namespace App\Tests\Mod\ArticleMiner\MockTraits;

use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Trait ValidatorInterfaceMock
 */
trait ValidatorInterfaceMock
{
    /**
     * Create validator mock
     *
     * @param int $countErrors
     * @return ValidatorInterface
     */
    private function getValidatorInterfaceMock(int $countErrors): ValidatorInterface
    {
        $mockCountable = \Mockery::mock(\Countable::class);
        $mockCountable->shouldReceive('count')
        ->andReturn($countErrors);

        $mockValidator = \Mockery::mock(ValidatorInterface::class);
        $mockValidator->shouldReceive('validate')
        ->andReturn($mockCountable);

        return $mockValidator;
    }
}
