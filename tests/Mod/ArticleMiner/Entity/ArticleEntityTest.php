<?php

namespace App\Tests\Mod\ArticleMiner\Entity;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Entity\ArticleEntity;

/**
 * Class ArticleEntityTest
 */
class ArticleEntityTest extends MockeryTestCase
{
    public function testEntityFactoryMethod()
    {
        $data = [
            'id' => 2,
            'title' => 'article 2 test',
            'url' => '/article/2/test',
            'createdDate' => new \DateTime('2019-01-01'),
        ];
        $entity = ArticleEntity::createByArray($data);

        $this->assertEquals($data, get_object_vars($entity));
    }

    public function testCacheableEntity()
    {
        $entity = new ArticleEntity();
        $entity->id = 33;

        $this->assertEquals('article.33', $entity->getCacheId());
    }
}
