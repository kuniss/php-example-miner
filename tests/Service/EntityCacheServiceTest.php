<?php

namespace App\Tests\Service;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Mod\ArticleMiner\Entity\ArticleEntity;
use App\Service\EntityCacheService;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class EntityCacheServiceTest
 */
class EntityCacheServiceTest extends MockeryTestCase
{
    public function testStoreEntityByAdapter()
    {
        $cacheTag = 'testTag';

        $entity = new ArticleEntity();
        $entity->id = 28;

        // mock cache item
        $mockCacheItem = \Mockery::mock(ItemInterface::class);
        $mockCacheItem->shouldReceive('isHit')
        ->andReturn(false);

        $mockCacheItem->shouldReceive('set')
        ->with(json_encode($entity))
        ->once();


        $mockCacheItem->shouldReceive('tag')
        ->with($cacheTag)
        ->once();

        // mock cache adapter
        $mockTagAwareAdapter = \Mockery::Mock(TagAwareAdapter::class);
        $mockTagAwareAdapter->shouldReceive('getItem')
        ->once()
        ->andReturn($mockCacheItem);

        $mockTagAwareAdapter->shouldReceive('save')
        ->with($mockCacheItem)
        ->once()
        ->andReturn(true);

        // init EntityCacheService
        $entityCacheService = new EntityCacheService($mockTagAwareAdapter);
        $storeResult = $entityCacheService->storeEntity($entity, $cacheTag, false);

        $this->assertTrue($storeResult);
    }
}
