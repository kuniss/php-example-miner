<?php

namespace App\Tests\Factory;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use App\Factory\EntityCacheServiceFactory;
use App\Service\EntityCacheService;

/**
 * Class EntityCacheServiceTest
 */
class EntityCacheServiceTest extends MockeryTestCase
{
    public function testFactoryMethod()
    {
        $init = EntityCacheServiceFactory::create();

        $this->assertEquals(get_class($init), EntityCacheService::class);
    }
}
