<?php

namespace App\Tests\Utils;

use \PHPUnit\Framework\TestCase;
use App\Utils\CurlRequest;

/**
 * Class CurlRequestTest
 */
class CurlRequestTest extends TestCase
{
    public function testCurlEnabled()
    {
        $this->assertTrue(function_exists('curl_init'));
        $this->assertTrue(function_exists('curl_setopt'));
        $this->assertTrue(function_exists('curl_exec'));
        $this->assertTrue(function_exists('curl_close'));
    }
}
